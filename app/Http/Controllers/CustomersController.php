<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;

class CustomersController extends Controller
{
  public function show($customerId)
  {
    $customer = Customer::find($customerId);
    //$customer = Customer::where('id', $customerId)->first();

    return view('frontend.customers.show')->with('customer', $customer);
  }

  public function index(Request $request)
  {
                            //LIKE esetén nem számítanak a kis/nagybetűk és az ékezetek sem.
    Customer::where('name', 'LIKE', '%Horvath%')//ahol a név tartalmazza, h 'horvath'
            ->orWhere('email', 'LIKE', '%Horvath%') //orWhere, elég ha egy feltétel teljesül
            ->orderBy('name', 'asc')
            ->skip(5) //1-5 kihagyja
            ->limit(10) // 10et ad vissza
            ->get();  //Collectionként adja vissza
            //->count(); Hány darab van

//utolsó elem:
Customer::orderBy('id', 'desc')->first();

            //10nél több az idja ÉS horvathnak hívják  VAGY 02-15.én regisztrált
            Customer::where(function($query){
              $query->where('id','>', 10)->where('name', 'LIKE', 'Horvath');
            })->orWhere('created_at', '=', '2021-02-15')
            ->get();

            Customer::whereBetween('created_at', ['2021-02-05', '2021-02-12']);

    $search = $request->input('search'); //array
    $customers = Customer::freshRegister()->search($search)->get();

  //  $customers = Customer::all();

    return view('frontend.customers.index')
            ->with('customers', $customers);
  }

  public function create()
  {
    $customer = new Customer;

    return view('frontend.customers.create')->with('customer', $customer);
  }

  public function store(Request $request)
  {
    $this->validate($request, [
      'name' => 'required',
      'email' => 'required|email|unique:customers,email',
      'password' => 'required|confirmed',
      'terms' => 'accepted'
    ]);

    $customer = new Customer;
    $customer->setAttributes($request->all());
    $customer->save();

    session()->flash('message', 'Köszöjük a regisztrációt');
    return redirect()->back();
  }

  public function edit($customerId)
  {
    $customer = Customer::findOrFail($customerId);

    return view('frontend.customers.edit')->with('customer', $customer);
  }

  public function update(Request $request, $customerId)
  {
    $this->validate($request, [
      'name' => 'required',
      'email' => 'required|email|unique:customers,email,'.$customerId,  //unique kivéve a megadott IDra
      'password' => 'confirmed'   //nem kötelező, de ha meg van adva, a 2jelszónak egyeznie kell
    ]);

    $customer = Customer::findOrFail($customerId);
    $customer->setAttributes($request->all());

    $customer->save();

    session()->flash('message', 'Az adatok módosultak');

    return redirect()->route('customers.index');
}

public function destroy($customerId)
{
    $customer = Customer::findOrFail($customerId);
    $customer->delete();

    return response()->json(['message' => 'Az ügyfél törölve']);
    //return redirect()->route('customers.index');
}




  /*  public function index()  //lista
    public function create() //létrehozás megjelenítése
    public function store() //adatbázisba mentés

    public function edit($id) //form megjelenítése
    public function update($id) //adatbázis művelet
    public function delete($id)
    public function show($id)*/


/*  public function index()
  {
    //1es id-ju customer
    $customer = Customer::find(1);

    dd($customer->lastUpdated());


    $customers = Customer::all();
    dd($customers);
  }*/

    public function newCustomer()
    {
      $customer = new Customer;
      $customer->name = 'Ödön';
      $customer->email = 'odon2@cim.hu';
      $customer->password = \Hash::make('1234');

      $customer->save();


      dd($customer);
    }

}
