$(document).ready(function(){
  $('.del-button').click(function() {
    var token = $(this).data('token');
    var url = $(this).data('url');
    var customerId = $(this).data('id');
    if(url !== undefined) {
      $.ajax({
         url: url,
         type: 'DELETE',
         data: { '_token': token},
         dataType:'json',
         success: function(data){
           if(data.message) {
             $('#customer-'+customerId).remove();
             alert(data.message);
           }
         }
      //  dataType: 'mycustomtype'
      });
    }
  });
});
