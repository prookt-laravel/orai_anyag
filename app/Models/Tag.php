<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Tag extends Model
{

  public function notes()
  { //több a többhöz kapcsolat                //ha kellenek időpontok is
    return $this->belongsToMany(Note::class)->withTimestamps();
  }
}
